const axios = require('axios');

const getServiceIndicador = async() => {
    
    try {
        const instance = axios.create({
            baseURL: `https://www.indecon.online/last`
        });
        
        const resp = await instance.get();
        return resp.data 

    } catch (error) {
        return false
    }
}

module.exports = {
    getServiceIndicador
}