const indicadorServ = require('../services/indicadorService');
const moment = require('moment'); // Se incorpora moment para convertir fecha unix a formato legible. 

const getElemento = async( req, res ) => {
    
    try {
        const data =  await indicadorServ.getServiceIndicador()    
        const dato = await data[`${ req }`]
    
        const nombre = dato.key;
        const valor = dato.value;
        const moneda = dato.unit;
        const fecha = moment(dato.fecha).format('DD-MM-YYYY');
    
        return {
            nombre,
            valor,
            moneda,
            fecha
        }
        
    } catch (error) {
        return false
    }
}

module.exports = {
    getElemento
}




