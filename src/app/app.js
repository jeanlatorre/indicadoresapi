import React, { Component } from 'react';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            indicador: '',
            data: '',
            dataFecha: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.formIndicadores = this.formIndicadores.bind(this);
    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({
          [name]: value
        });
      }
    formIndicadores(e) {
        var tipo = this.state.indicador;
        e.preventDefault();
        fetch(`/api/indicador/${ tipo}`)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.moneda == "dolar"){
                data.moneda = "USD"
            } else  if (data.moneda == "porcentual"){
                data.moneda = " %"
            }

            if (typeof data.nombre !== 'undefined')
            {
                this.setState({
                    data: `\n El valor de ${ data.nombre } es de ${ data.valor} ${ data.moneda }`,
                    dataFecha : `valor corresponde a la fecha : ${ data.fecha } `
                })
            } else {
                this.setState({
                    data: `\n No se encontraron registros para "${ tipo }"`,
                    dataFecha: ''
                })
            }

        });
    
      }

    render() {
        return (
            <div>
                <nav className="light-blue darken-4">
                    <div className="container">
                        <a className="brand-logo center-align" href="/">Indicadores Económicos</a>
                    </div>
                </nav>
            

                <div className="container">
                    <div className="row">
                        <div className="col s12">
                            <div className="card">
                                <div className="card-content">
                                    
                                    <form onSubmit={this.formIndicadores}>
                                        <div className="row">
                                            <div className="input-field col s4">
                                                <input name="indicador" onChange={this.handleChange} value={this.state.indicador} type="text" placeholder="Ingrese Indicador" autoFocus/>
                                            </div>
                                            <div className="input-field col s4">
                                                <button type="submit" className="btn light-blue darken-4">
                                                    Buscar 
                                                </button>  
                                            </div>
                                        </div>
                                       
                                    </form>
                                    <h5>{this.state.data}</h5>
                                    <h6>{this.state.dataFecha}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default App;