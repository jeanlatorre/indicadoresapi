const express = require('express');
const router = express.Router();
const indicadorCtrl = require('../controllers/indicadorController');



// Inicio
router.get('/', (req, res) => {
    res.json({
        status: 'API ARRIBA'
    });
});

// Indicadores
router.get('/indicador/:tipo', async (req, res) => {
    const indicador = await indicadorCtrl.getElemento(req.params.tipo);
    //res.send(`Indicador ${ indicador.valor }`)
    res.json({
        nombre: indicador.nombre,
        fecha: indicador.fecha,
        valor: indicador.valor,
        moneda: indicador.moneda
    })
})


module.exports = router;