'use strict'
const express = require('express')
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser')

const app = express()
const port = process.env.PORT || 3000

app.use(morgan('dev'))
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: false }))

// Routes
app.use('/api',require('./routes/indicadoresRoutes'));

// File public
app.use(express.static(path.join(__dirname, 'public')));

// SERVER UP
app.listen(port, () => {
    console.log(`API REST INDICADORES ECONOMICOS - PUERTO  ${ port } `)
})

