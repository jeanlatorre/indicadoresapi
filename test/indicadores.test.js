const indicadorCtrl = require('../src/controllers/indicadorController');
const expect = global.expect;

describe('indicadores', () => {
    test('Debe incluir un tipo de indicador', async () => {
        const indicador = "cobre"
        const res = await indicadorCtrl.getElemento(indicador)
        expect(res).not.toBeNull()
    });
});

