# API Indicadores Económicos
Node.js + ExpressJS + React + NODEMON + AXIOS + BABEL + JEST + MOMENT 

------------
# ESPECIFICACIONES 
- **NODE** Version 14.0.1
- **REACT** Version 16.13.1
- **EXPRESS** Para el servidor.
- **JEST** Para generar pruebas.
- **Nodemon** para el desarrollo.
- **ES6** para promesas y rutas.
- **AXIOS** para consumir api.


## ESTRUCTURA
```bash
    deploy (Archivos para despliegue desde gitlab)
    /src
        /app 
        /controllers
        /public
        /routes
        /services
        index.js 
    /test (Pruebas)
    .gitgnore 
    .babelrc
    package.json (Archivo de configuraciòn principal del APP)
    .webpack.config.js
```


# EVIDENCIA POSTMANT - END POINTS
- Abrir en el navegador

  https://documenter.getpostman.com/view/9168650/SzmjyaXi

# INSTALL
1. Clonar Repositorio.
		
		git@gitlab.com:Jeanl/indicadoresapi.git
		
2. Instalar Dependencias.

		npm install
		

# INICIAR APLICACIÓN.

### Webpack: Iniciar webpack con nodemon para generar bundle.js
    npm run webpack
### Desarrollo: Inicia proyecto con nodemon.
	npm run dev
### Test: Inicia las pruebas.
	npm run test
